let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

if (mix.inProduction()) mix.version();

mix.disableNotifications();

mix.webpackConfig({
    devServer: {
        host: "0.0.0.0",
        port: 8080
    }
});

mix.browserSync({
    proxy: 'nginx',
    open: false,
    notify: false
});
