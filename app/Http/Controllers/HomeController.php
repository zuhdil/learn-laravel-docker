<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Contracts\View\View;

class HomeController extends Controller
{
    /**
     * @var ViewFactory
     */
    private $view;

    /**
     * Create a new controller instance.
     */
    public function __construct(ViewFactory $view)
    {
        $this->view = $view;

        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     */
    public function index(): View
    {
        return $this->view->make('home');
    }
}
