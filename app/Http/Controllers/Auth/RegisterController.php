<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\UserWelcomeGreeting;
use App\User;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\Factory as ValidatorFactory;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * @var ValidatorFactory
     */
    private $validator;

    /**
     * @var User
     */
    private $model;

    /**
     * @var Mailer
     */
    private $mailer;

    public function __construct(ValidatorFactory $validator, User $model, Mailer $mailer)
    {
        $this->middleware('guest');

        $this->validator = $validator;
        $this->model = $model;
        $this->mailer = $mailer;
    }

    /**
     * Get a validator for an incoming registration request.
     */
    protected function validator(array $data): Validator
    {
        return $this->validator->make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     */
    protected function create(array $data): User
    {
        $user = $this->model->newQuery()->create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        $this->mailer->to($user->email)->queue(new UserWelcomeGreeting($user));

        return $user;
    }

    /**
     * Where to redirect users after registration.
     */
    public function redirectTo(): string
    {
        return route('home');
    }
}
