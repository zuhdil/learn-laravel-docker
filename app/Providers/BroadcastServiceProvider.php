<?php

namespace App\Providers;

use Illuminate\Contracts\Broadcasting\Factory as Broadcast;
use Illuminate\Support\ServiceProvider;

/**
 * @codeCoverageIgnore
 */
class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @psalm-suppress UnresolvableInclude
     */
    public function boot(): void
    {
        $this->app->make(Broadcast::class)->routes();

        require base_path('routes/channels.php');
    }
}
