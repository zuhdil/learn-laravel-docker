<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__.'/app')
    ->in(__DIR__.'/config')
    ->in(__DIR__.'/database')
    ->in(__DIR__.'/resources/lang')
    ->in(__DIR__.'/routes')
    ->in(__DIR__.'/tests');

return PhpCsFixer\Config::create()
    ->setRules([
        '@Symfony' => true,
        'braces' => ['allow_single_line_closure' => false],
    ])
    ->setUsingCache(false)
    ->setFinder($finder);

