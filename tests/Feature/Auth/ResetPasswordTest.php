<?php

namespace Tests\Feature\Auth;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Tests\TestCase;

class ResetPasswordTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function get_reset_password_page_should_return_ok_response(): void
    {
        $response = $this->get(route('password.reset', ['token' => 'foo']));

        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function get_reset_password_page_should_pass_token_to_view(): void
    {
        $response = $this->get(route('password.reset', ['token' => 'foo']));

        $response->assertViewHas('token', 'foo');
    }

    /**
     * @test
     */
    public function post_empty_reset_password_request_should_return_redirect_back_response(): void
    {
        $this->get(route('password.reset', ['token' => 'foo']));
        $response = $this->post(route('password.request', []));

        $response->assertRedirect(route('password.reset', ['token' => 'foo']));
    }

    /**
     * @test
     */
    public function post_empty_reset_password_request_should_return_response_with_validation_error(): void
    {
        $this->get(route('password.reset', ['token' => 'foo']));
        $response = $this->post(route('password.request', []));

        $response->assertSessionHasErrors(['token', 'email', 'password']);
    }

    /**
     * @test
     */
    public function post_reset_password_request_with_invalid_token(): void
    {
        $user = factory(User::class)->create();
        $password = 'secret';

        $response = $this->post(route('password.request', [
            'token' => 'invalid',
            'email' => $user->email,
            'password' => $password,
            'password_confirmation' => $password,
        ]));

        $response->assertSessionHasErrors(['email']);
    }

    /**
     * @test
     */
    public function post_reset_password_request_with_invalid_email(): void
    {
        $user = factory(User::class)->create();
        $password = 'secret';

        $response = $this->post(route('password.request', [
            'token' => Password::createToken($user),
            'email' => 'invalid.'.$user->email,
            'password' => $password,
            'password_confirmation' => $password,
        ]));

        $response->assertSessionHasErrors(['email']);
    }

    /**
     * @test
     */
    public function post_reset_password_request_with_unconfirmed_password(): void
    {
        $user = factory(User::class)->create();

        $response = $this->post(route('password.request', [
            'token' => Password::createToken($user),
            'email' => $user->email,
            'password' => 'secret',
            'password_confirmation' => '123456',
        ]));

        $response->assertSessionHasErrors(['password']);
    }

    /**
     * @test
     */
    public function post_valid_reset_password_request_should_return_redirect_response(): void
    {
        $user = factory(User::class)->create();
        $password = 'secret';

        $response = $this->post(route('password.request', [
            'token' => Password::createToken($user),
            'email' => $user->email,
            'password' => $password,
            'password_confirmation' => $password,
        ]));

        $response->assertRedirect(route('home'));
    }

    /**
     * @test
     */
    public function post_valid_reset_password_request_should_update_user_password(): void
    {
        $user = factory(User::class)->create();
        $newPassword = 'new password';

        $this->post(route('password.request', [
            'token' => Password::createToken($user),
            'email' => $user->email,
            'password' => $newPassword,
            'password_confirmation' => $newPassword,
        ]));

        $hashPassword = User::find($user->id)->password;
        $this->assertTrue(Hash::check($newPassword, $hashPassword));
    }
}
