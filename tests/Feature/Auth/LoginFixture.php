<?php

namespace Tests\Feature\Auth;

use App\User;

class LoginFixture
{
    public const PASSWORD = 'secret';

    /**
     * @var User
     */
    private $user;

    public function user(): User
    {
        if (!$this->user instanceof User) {
            $this->user = factory(User::class)->create(['password' => bcrypt(self::PASSWORD)]);
        }

        return $this->user;
    }

    public function loginParams(): array
    {
        return [
            'email' => $this->user()->email,
            'password' => self::PASSWORD,
        ];
    }
}
