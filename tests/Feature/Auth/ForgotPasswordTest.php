<?php

namespace Tests\Feature\Auth;

use App\User;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class ForgotPasswordTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function get_forgot_password_page_should_return_ok_response(): void
    {
        $response = $this->get(route('password.request'));

        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function post_empty_request_should_return_redirect_back_response(): void
    {
        $this->get(route('password.request'));
        $response = $this->post(route('password.email'), []);

        $response->assertRedirect(route('password.request'));
    }

    /**
     * @test
     */
    public function post_empty_request_should_return_response_with_validation_error(): void
    {
        $response = $this->post(route('password.email'), []);

        $response->assertSessionHasErrors(['email']);
    }

    /**
     * @test
     */
    public function post_with_unregistered_email_should_return_validation_error_response(): void
    {
        $response = $this->post(route('password.email'), ['email' => 'foo@example.com']);

        $response->assertSessionHasErrors(['email']);
    }

    /**
     * @test
     */
    public function post_with_registered_email_should_return_response_with_success_flash_message(): void
    {
        $user = factory(User::class)->create();
        $response = $this->post(route('password.email'), ['email' => $user->email]);

        $response->assertSessionHas('status');
    }

    /**
     * @test
     */
    public function post_valid_forgot_password_request_should_send_reset_password_notification(): void
    {
        Notification::fake();

        $user = factory(User::class)->create();
        $this->post(route('password.email'), ['email' => $user->email]);

        Notification::assertSentTo($user, ResetPassword::class);
    }
}
