<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function get_login_page_should_return_ok_response(): void
    {
        $response = $this->get(route('login'));

        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function post_invalid_login_should_return_redirect_back_response(): void
    {
        $this->get(route('login'));
        $response = $this->post(route('login'), []);

        $response->assertRedirect(route('login'));
    }

    /**
     * @test
     */
    public function post_invalid_login_should_return_response_with_validation_error(): void
    {
        $response = $this->post(route('login'), []);

        $response->assertSessionHasErrors(['email', 'password']);
    }

    /**
     * @test
     */
    public function post_valid_login_should_return_redirect_response(): void
    {
        $fixture = new LoginFixture();

        $response = $this->post(route('login'), $fixture->loginParams());

        $response->assertRedirect(route('home'));
    }

    /**
     * @test
     */
    public function post_valid_login_should_log_user_in(): void
    {
        $fixture = new LoginFixture();

        $this->post(route('login'), $fixture->loginParams());

        $this->assertEquals(Auth::user()->id, $fixture->user()->id);
    }

    /**
     * @test
     */
    public function should_bypass_login_page_if_already_authenticated(): void
    {
        $fixture = new LoginFixture();

        $response = $this->actingAs($fixture->user())
            ->get(route('login'));

        $response->assertRedirect(route('home'));
    }
}
