<?php

namespace Tests\Feature\Auth;

use App\Mail\UserWelcomeGreeting;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function get_register_page_should_return_ok_response(): void
    {
        $response = $this->get(route('register'));

        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function post_invalid_registration_should_return_redirect_back_response(): void
    {
        $this->get(route('register'));
        $response = $this->post(route('register'), []);

        $response->assertRedirect(route('register'));
    }

    /**
     * @test
     */
    public function post_invalid_registration_should_return_response_with_validation_error(): void
    {
        $response = $this->post(route('register'), []);
        $response->assertSessionHasErrors(['name', 'email', 'password']);
    }

    private function registerParams(): array
    {
        return [
            'name' => 'foo',
            'email' => 'foo@example.com',
            'password' => 'secret',
            'password_confirmation' => 'secret',
        ];
    }

    /**
     * @test
     */
    public function post_invalid_registration_without_password_confirmation(): void
    {
        $params = array_merge($this->registerParams(), ['password_confirmation' => '']);

        $response = $this->post(route('register'), $params);

        $response->assertSessionHasErrors(['password']);
    }

    /**
     * @test
     */
    public function post_valid_registration_with_invalid_email(): void
    {
        $params = array_merge($this->registerParams(), ['email' => 'foo.mail.com']);

        $response = $this->post(route('register'), $params);

        $response->assertSessionHasErrors(['email']);
    }

    /**
     * @test
     */
    public function post_valid_registration_with_email_already_exists(): void
    {
        $params = $this->registerParams();
        factory(User::class)->create(['email' => $params['email']]);

        $response = $this->post(route('register'), $params);

        $response->assertSessionHasErrors(['email']);
    }

    /**
     * @test
     */
    public function post_valid_registration_should_return_redirect_response(): void
    {
        $response = $this->post(route('register'), $this->registerParams());

        $response->assertRedirect(route('home'));
    }

    /**
     * @test
     */
    public function post_valid_registration_should_create_new_user(): void
    {
        $params = $this->registerParams();

        $this->post(route('register'), $params);

        $this->assertInstanceOf(User::class, User::where('email', $params['email'])->first());
    }

    /**
     * @test
     */
    public function post_valid_registration_should_log_user_in(): void
    {
        $this->post(route('register'), $this->registerParams());

        $this->assertNotNull(Auth::user());
    }

    /**
     * @test
     */
    public function post_valid_registration_should_send_welcome_greeting_email(): void
    {
        Mail::fake();

        $this->post(route('register'), $this->registerParams());

        $user = User::first();

        Mail::assertQueued(UserWelcomeGreeting::class, function ($mail) use ($user) {
            return $mail->hasTo($user->email);
        });
    }
}
