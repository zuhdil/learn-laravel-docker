<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HomeTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function unauthenticated_user_access_should_redirect_to_login_page(): void
    {
        $response = $this->get(route('home'));

        $response->assertRedirect(route('login'));
    }

    /**
     * @test
     */
    public function authenticated_user_should_have_access(): void
    {
        $response = $this->actingAs(factory(User::class)->create())
            ->get(route('home'));

        $response->assertSuccessful();
    }
}
